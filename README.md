# Regexle

A small game for practicing regexes. Inspired by [Zendo](https://en.wikipedia.org/wiki/Zendo_(game)).

The game: Guess the secret regular expression by entering strings; the game will tell you whether they match.

Feel free to suggest regexes!

### License

GNU General Public License v3.0 or later
