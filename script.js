Array.prototype.sample = function () {
    return this[Math.floor(Math.random() * this.length)]
}
Array.prototype.difference = function (arr2) {
    return this.filter((x) => !arr2.includes(x))
}
Set.prototype.filter = function (fn) {
    return new Set([...this].filter(fn))
}
Set.prototype.sample = function () {
    return [...this].sample()
}
Set.prototype.slowdifference = function (set2) {
    return new Set([...this].filter((x) => !set2.has(x)))
}
Set.prototype.difference = function (set2) {
    const setA = new Set(this)
    
    for (const v of set2.values()) {
        setA.delete(v)
    }
    
    return setA
}
String.prototype.escapeHtml = function () {
    return this.replaceAll("&", "&amp;")
        .replaceAll("<", "&lt;")
        .replaceAll(">", "&gt;")
        .replaceAll('"', "&quot;")
        .replaceAll("'", "&#039;")
}

let stringHistory = {
    strings: [""],
    index: 0,
}

let ruleHistory = {
    strings: [""],
    index: 0,
}

let exercises = ["two|three|five|seven"]

let stringGuesses = 0
let regexGuesses = 0

document.addEventListener(
    "DOMContentLoaded",
    function (event) {
        const strings = new Set(english)

        const latestdiv = document.querySelector("#latest")
        const guessBox = document.querySelector("#guess")
        const ruleGuessBox = document.querySelector("#rule-guess")
        const newPatternBox = document.querySelector("#new-pattern")
        const newPatternButton = document.querySelector("#new-pattern-button")
        const matchingAnswerdiv = document.querySelector("#matching")
        const nonmatchingAnswerdiv = document.querySelector("#non-matching")
        const regexGuessesdiv = document.querySelector("#guesses")
        const closePopupButton = document.querySelector("#close-button")
        const openPopupButton = document.querySelector("#guess-pattern-button")
        const giveUpButton = document.querySelector("#give-up-button")

        window.onkeydown = (event) => {
            if (event.key === "Escape") {
                closePopup()
            }
        }
        
        guessBox.onkeydown = (event) => {
            if (event.key === "Enter") {
                submit()
            } else if (event.key === "ArrowUp") {
                event.preventDefault()
                scrollHistory(-1, stringHistory, guessBox)
            } else if (event.key === "ArrowDown") {
                event.preventDefault()
                scrollHistory(1, stringHistory, guessBox)
            }
        }
        
        ruleGuessBox.onkeydown = (event) => {
            if (event.key === "Enter") {
                submitRule()
            } else if (event.key === "ArrowUp") {
                event.preventDefault()
                scrollHistory(-1, ruleHistory, ruleGuessBox)
            } else if (event.key === "ArrowDown") {
                event.preventDefault()
                scrollHistory(1, ruleHistory, ruleGuessBox)
            }
        }
        closePopupButton.onclick = (event) => {
            closePopup()
        }
        openPopupButton.onclick = (event) => {
            openPopup()
        }
        giveUpButton.onclick = (event) => {
            giveUp()
        }
        newPatternButton.onclick = (event) => {
            createNewPattern()
        }
        newPatternBox.onkeydown = (event) => {
            if (event.key === "Enter") {
                createNewPattern()
            }
        }
        const queryString = window.location.search.substring(1)
        let regexpattern = decodeURIComponent(atob(queryString))
        let regexpatternAsRegex
        
        try {
            regexpatternAsRegex = new RegExp(regexpattern)
        } catch (e) {
            alert(
                `${e.message.escapeHtml()}. We will redirect you to the default regular expression.`,
            )
            window.location.href = window.location.origin
            return
        }
        
        const usingDailyRegex = !queryString
        let day
        if (!queryString) {
            const now = new Date()
            const start = new Date(2024, 4, 15)
            const timeDiff = now.getTime() - start.getTime()
            day = Math.ceil(timeDiff / (1000 * 60 * 60 * 24))
            puzzleIndex = (day-1) % PUZZLES.length
            regexpattern = PUZZLES[puzzleIndex]
        }
        
        let matchingStrings = strings.filter((w) => w.match(regexpattern) !== null)
        let nonmatchingStrings = strings.filter(
            (w) => w.match(regexpattern) === null,
        )
        
        if (matchingStrings.size === 0) {
            alert(
                "The specified pattern does not match any words in our dictionary. This will not be a fun game :/ We will redirect you to the default regular expression.",
            )
            window.location.href = window.location.origin
            return
        }
        if (nonmatchingStrings.size === 0) {
            alert(
                "The specified pattern matches all words in our dictionary. This will not be a fun game :/ We will redirect you to the default regular expression.",
            )
            window.location.href = window.location.origin
            return
        }
        
        guessBox.focus()
        
        // Give two starting examples.
        //matchingAnswerdiv.innerHTML = matchingStrings.sample() + "<br>"
        //nonmatchingAnswerdiv.innerHTML = nonmatchingStrings.sample() + "<br>"
        
        //addExamples()
        
        function normalizeRegex(regex) {
            return regex.replaceAll("/", "")
        }
        
        function submitRule() {
            let value = normalizeRegex(ruleGuessBox.value)
            regexGuessesdiv.innerHTML = `${value.escapeHtml()}<br>${regexGuessesdiv.innerHTML}`
            
            ruleHistory.strings.splice(1, 0, value)
            ruleHistory.index = 0
            ruleGuessBox.value = ""
            let guessPattern
            
            regexGuesses += 1
            
            try {
                guessPattern = new RegExp(value)
            } catch (e) {
                latestdiv.classList = "complaining"
                latestdiv.innerHTML = e.message
                closePopup()
                return
            }
            let guessMatchingStrings = strings.filter(
                (w) => w.match(guessPattern) !== null,
            )
            let guessNonmatchingStrings = strings.filter(
                (w) => w.match(guessPattern) === null,
            )
            let matchingUnexpectedly =
                matchingStrings.difference(guessMatchingStrings) // das sind alle zutreffenden, ohne die, bei denen das Spiely schon vermutet, dass sie zutreffen
                //These are all the correct ones, excluding those that the game already suspects are correct
            let nonmatchingUnexpectedly = nonmatchingStrings.difference(
                guessNonmatchingStrings,
            ) // das sind alle nicht-zutreffenden, ohne die, bei denen es schon vermutet, dass sie nicht zutreffen
            //These are all the non-applicable ones, excluding those for which it is already suspected that they do not apply
            
            //console.log("whole sets", guessMatchingStrings, guessNonmatchingStrings)
            //console.log("differences", matchingUnexpectedly, nonmatchingUnexpectedly)
            
            if (
                matchingUnexpectedly.size === 0 &&
                nonmatchingUnexpectedly.size === 0
            ) {
                endGame()
                latestdiv.classList = "affirmative"
                let string = usingDailyRegex ? `day ${day}'s` : "the"
                latestdiv.innerHTML = `<b>Correct! <code>${regexpattern}</code> is ${string} secret regex!</b>`
                
                latestdiv.innerHTML += ` You guessed ${stringGuesses} string${stringGuesses !== 1?"s":""}`
                latestdiv.innerHTML += ` and ${regexGuesses} regex${regexGuesses !== 1?"es":""}.`
                const audio = new Audio("sounds/win.mp3")
                audio.volume = 0.4
                audio.play()
                confetti({
                    particleCount: 100,
                    origin: {x: 0},
                    angle: 20,
                    spread: 55,
                    startVelocity: 60,
                })
            } else {
                if (nonmatchingUnexpectedly.size > 0) {
                    const adverseExample = nonmatchingUnexpectedly.sample()
                    latestdiv.classList = "complaining"
                    latestdiv.innerHTML = `Incorrect! According to your guess, <code>${adverseExample.escapeHtml()}</code> would match the pattern, but it does not.`
                    nonmatchingAnswerdiv.innerHTML =
                        `${adverseExample}<br>${matchingAnswerdiv.innerHTML}`
                } else {
                    const adverseExample = matchingUnexpectedly.sample()
                    latestdiv.classList = "complaining"
                    latestdiv.innerHTML = `Incorrect! According to your guess, "${adverseExample.escapeHtml()}" would not match the pattern, but it does.`
                    matchingAnswerdiv.innerHTML =
                        `${adverseExample}<br>${matchingAnswerdiv.innerHTML}`
                }
                const audio = new Audio("sounds/wrong.mp3")
                audio.play()
            }
            closePopup()
        }
        
        function submit() {
            let value = guessBox.value
            stringHistory.strings.splice(1, 0, value)
            stringHistory.index = 0
            guessBox.value = ""
            
            stringGuesses += 1
            
            let ergebnis = value.match(regexpattern)
            if (ergebnis !== null) {
                let antwort = `${value.escapeHtml()}<br>`
                matchingAnswerdiv.innerHTML =
                    antwort + matchingAnswerdiv.innerHTML
                latestdiv.classList = "affirmative"
                latestdiv.innerHTML = `The string ${value.escapeHtml()} matches the secret pattern.`
            } else {
                let antwort = `${value.escapeHtml()}<br>`
                nonmatchingAnswerdiv.innerHTML =
                    antwort + nonmatchingAnswerdiv.innerHTML
                latestdiv.classList = "complaining"
                latestdiv.innerHTML = `The string ${value.escapeHtml()} does <b>not</b> match the secret pattern.`
            }
        }
        function encodeRegex(regex) {
            let uriEncodedPattern = encodeURIComponent(regex)
            let encodedPattern64 = btoa(uriEncodedPattern)
            return encodedPattern64
        }
        
        function createNewPattern() {
            let pattern = normalizeRegex(newPatternBox.value)
            if (pattern !== "") {
                location = "?" + encodeRegex(pattern)
            }
        }
        function scrollHistory(direction, history, inputBox) {
            if (history.strings.length === 0) {
                return
            }
            
            history.index -= direction
            if (history.index < 0) {
                history.index = 0
            }
            if (history.index > history.strings.length - 1) {
                history.index = history.strings.length - 1
            }
            inputBox.value = history.strings[history.index]
            inputBox.selectionStart = inputBox.selectionEnd =
                inputBox.value.length
        }
        function closePopup() {
            document.querySelector("#guess-popup").style.display = "none"
            guessBox.focus()
        }
        function openPopup() {
            document.querySelector("#guess-popup").style.display = "flex"
            ruleGuessBox.focus()
        }
        function giveUp() {
            if (
                confirm(
                    "Are you sure you want to give up and see the solution?",
                )
            ) {
                endGame()
                latestdiv.classList = "complaining"
                let string = usingDailyRegex ? `for day ${day} ` : ""
                latestdiv.innerHTML = `<b>You gave up. The secret regex ${string}is: <code>${regexpattern.escapeHtml()}</code></b>`
            }
        }
        function endGame() {
            guessBox.disabled = true
            openPopupButton.disabled = true
            giveUpButton.disabled = true
        }
        /*function addExamples() {
            const exampleDiv = document.querySelector("#example-patterns")
            let i = 1
            for (example of examples) {
                const link = document.createElement("a")
                link.target = "_blank"
                link.href = "/?" + encodeRegex(example)
                link.innerHTML = i
                i += 1
                exampleDiv.appendChild(link)
                if (example !== examples[examples.length - 1]) {
                    exampleDiv.innerHTML += " "
                }
            }
        }*/
    },
    false,
)
